from keboola import docker
import requests
import base64
import json
import csv

# initialize application
cfg = docker.Config('/data/')
params = cfg.get_parameters()

# access the supplied values
COMMUNITY = cfg.get_parameters()['COMMUNITY']
RESOURCE = cfg.get_parameters()['RESOURCE']
USERNAME = cfg.get_parameters()['USERNAME']
PASSWORD = cfg.get_parameters()['PASSWORD']


def get_variables(auth_code):
    """Get the list of available variables and their corresponding parameters"""
    
    URL = str("https://api.visioncritical.com/v1/applications/"+COMMUNITY+"/memberVariables")
    
    PAYLOAD = {
        'limit': '1000'
        ,'sort':'name:a'
        }
    
    HEADERS = {
        "authorization": auth_code
        ,"cache-control": "no-cache"
        #,"X-WebApi- Return-Resource":"true"
        }
    
    print "sending request.."
    response = requests.request("GET", URL, headers=HEADERS, params=PAYLOAD)
    
    dataRaw = json.loads(response.text)
    print "got data.."
    
    data = dataRaw[u'items']
    
    data_file = open('/data/out/tables/data.csv', 'w')
    csvwriter = csv.writer(data_file)
    count = 0
    for emp in data:
          if count == 0:
                 header = emp.keys()
                 csvwriter.writerow(header)
                 count += 1
          csvwriter.writerow(emp.values())
    
    data_file.close()


encoded = base64.b64encode(str(USERNAME + ":" + PASSWORD))
auth_code = str("Basic " + encoded)
get_variables(auth_code)
